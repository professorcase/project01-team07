﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Student
    {
    
        [ScaffoldColumn(false)]
        public int StudentID { get; set; }
        [Required]
        public string email { get; set; }
        [Display(Name = "FirstName")]
        public string firstName { get; set; }
        [Display(Name = "LastName")]
        public string lastName { get; set; }
        public bool isEnrolled { get; set; }
      
        public Student()
        {
            isEnrolled = false;
        }



    }
}






