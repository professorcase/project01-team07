﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class FeedbackHistory
    {
        [ScaffoldColumn(false)]
        public int FeedbackHistoryID { get; set; }
        [Required]
        public string StudentID { get; set; }
        [Required]
        public string type { get; set; }
        [Required]
        public string crn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedUtc { get; set; }
    }
}
