﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Section
    {
        public Section()
        {
            isAssigned = false;
        }
        [ScaffoldColumn(false)]
        public int SectionID { get; set; }
        [Required]
        [Display(Name = "SectionName")]
        public string sectionName { get; set; }
        [Display(Name = "SectionCrn")]
        public string sectionCRN { get; set; }

        public bool isAssigned { get; set; }

    }
}
