﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Feedback
    {
        [ScaffoldColumn(false)]
        public int FeedbackID { get; set; }
        [Required]
        public string FeedbackResponse { get; set; }

        public string FeedbackComment { get; set; }

        public string type { get; set; }

        public string crn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd H:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime CreatedUtc { get; set; }

    }
}
