﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Course
    {
        [ScaffoldColumn(false)]
        public int CourseID { get; set; }
        [Required]
        [Display(Name = "CourseName")]
        public string courseName { get; set; }
    }
}
