﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Faculty
    {
        [ScaffoldColumn(false)]
        public int FacultyID { get; set; }
        [Required]
        [Display(Name = "LastName")]
        public string lastName { get; set; }
        [Display(Name = "FirstName")]
        public string firstName { get; set; }
       
      
        [Required]
        public string email { get; set; }
        public string departmentname { get; set; }
        public bool isFacultyEnrolled { get; set; }

        public Faculty()
        {
            isFacultyEnrolled = false;
        }

        public virtual ApplicationUser ApplicationUser { get; set; }

       
    }
}

