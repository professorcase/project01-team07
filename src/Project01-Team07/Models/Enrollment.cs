﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project01_Team07.Models
{
    public class Enrollment
    {
        [ScaffoldColumn(false)]
        public int EnrollmentID { get; set; }
        public string semester { get; set; }
        public int crn1 { get; set; }
        public int crn2 { get; set; }
        public int crn3 { get; set; }
        public int StudentID { get; set; }
     //   public virtual Student Student { get; set; }



    }
}


