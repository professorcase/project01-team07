$(document).ready(function(){

    "use strict";

    // ORDER DOES MATTER

    // FULLSCREEN BLOCK HEIGHTS ------------------------------------------------
    function setMinHeightAsWindowsHeight($item) {
        var windowHeight = $(window).height();
        $item.css({minHeight: windowHeight});
    }


    // FLEXSLIDER --------------------------------------------------------------
    var $sliderIntro = $('#slider-intro');
    if ($sliderIntro.length) {
        $sliderIntro.flexslider({
            animation: 'fade',
            directionNav: false,
            animationLoop: false,
            slideshow: false
        });
        setMinHeightAsWindowsHeight($sliderIntro);
        $(window).resize(function(){setMinHeightAsWindowsHeight($sliderIntro)});
        $.each($sliderIntro.find('.slide'), function (index, value){
            setMinHeightAsWindowsHeight($(value));
            $(window).resize(function(){setMinHeightAsWindowsHeight($(value))});
        });
    }

    var $sliderAbout = $('#slider-about');
    if ($sliderAbout.length) {
        $sliderAbout.flexslider({
            animation: 'fade',
            directionNav: false,
            slideshow: false
        });
    }

    var $sliderTestimonials = $('#slider-testimonials');
    if ($sliderTestimonials.length) {
        $sliderTestimonials.flexslider({
            animation: 'slide',
            directionNav: false,
            slideshow: false
        });
    }

    var $sliderPosts = $('#slider-posts');
    if ($sliderPosts.length) {
        $sliderPosts.flexslider({
            animation: 'slide',
            animationLoop: false,
            controlNav: false,
            directionNav: true,
            prevText: "<span class=\"icon fa fa-angle-left\"></span>",
            nextText: "<span class=\"icon fa fa-angle-right\"></span>",
            slideshow: false,
            itemWidth: 360,
            itemMargin: 30
        });
    }

    // PRETTY PHOTO ------------------------------------------------------------
    var prettyPhotoOptions = {
        hook: 'data-gal',
        default_width: '1500',
        default_height: '1000',
        social_tools: '',
        markup: '<div class="pp_pic_holder"> \
                    <div class="ppt">&nbsp;</div> \
                    <div class="pp_content_container"> \
                        <div class="pp_content"> \
                            <div class="pp_loaderIcon"></div> \
                            <div class="pp_fade"> \
                                <div class="pp_hoverContainer"> \
                                    <a class="pp_next" href="#"><span class="icon fa fa-angle-right"></span></a> \
                                    <a class="pp_previous" href="#"><span class="icon fa fa-angle-left"></span></a> \
                                </div> \
                                <div id="pp_full_res"></div> \
                                <div class="pp_details"> \
                                    <p class="pp_description"></p> \
                                    {pp_social} \
                                </div> \
                                <a class="pp_close" href="#"><span class="icon fa fa-times"></span></a> \
                            </div> \
                        </div> \
                    </div> \
                </div> \
                <div class="pp_overlay"></div>',
        gallery_markup: '<div class="pp_gallery"> \
                            <a href="#" class="pp_arrow_previous"><span class="icon fa fa-angle-left"></span></a> \
                            <div> \
                                <ul> \
                                    {gallery} \
                                </ul> \
                            </div> \
                            <a href="#" class="pp_arrow_next"><span class="icon fa fa-angle-right"></span></a> \
                        </div>'
    }
    $("a[data-gal='prettyPhoto']").prettyPhoto(prettyPhotoOptions);

    // PAGE-NAV ----------------------------------------------------------------
    function highlightActivePageNavItem() {
        var currentTopOffset = $(window).scrollTop();
        var panelsOffset = [];
        var stock = $(window).height() / 5;
        $.each($('.page-panel'), function(index, value) {
            var panelId = $(value).attr('id');
            var $panel = $("#" + panelId);
            panelsOffset[panelId] = {'top': $panel.offset().top, 'bottom': ( $panel.offset().top + $panel.outerHeight())}
        });

        for (var index in panelsOffset) {
            if (currentTopOffset + stock >= panelsOffset[index]['top'] && currentTopOffset + stock <= panelsOffset[index]['bottom']) {
                if (! $pageNav.find('#' + index).hasClass('active')) {
                    $pageNav.find('li').removeClass('active');
                    $pageNav.find('#' + index.replace('page-panel', 'page-nav')).addClass('active');
                    return false;
                }
            }
        }
        return true;
    }
    function scrollToPanel(link) {
        var panelId = link.href.split("#")[1];
        var panelOffset = $("#"+panelId).offset();
        $('html, body').animate({ scrollTop: panelOffset.top }, 800);
        $pageNav.find('li').removeClass('active');
        $(link).closest('li').addClass('active');
    }

    var $pageNav = $('#page-nav');
    if ($pageNav.hasClass('page-nav-dynamic')) {
        $pageNav.find('a').click(function(event){
            event.preventDefault();
            scrollToPanel(this);
        });
        $(window).scroll(highlightActivePageNavItem);
        highlightActivePageNavItem();
    }


    // BTN BEGIN STORY ---------------------------------------------------------
    // scroll to "about me" panel
    var $btnBeginStory = $('#btn-begin-story');
    if ($btnBeginStory.length) {
        $btnBeginStory.find('a').click(function () {
            event.preventDefault();
            scrollToPanel(this);
        });
    }

    // PAGE-PANEL-INTRO --------------------------------------------------------
    var $pagePanelInto = $('#page-panel-intro');
    setMinHeightAsWindowsHeight($pagePanelInto);
    $(window).resize(function(){setMinHeightAsWindowsHeight($pagePanelInto)});

    // PANEL-PORTFOLIO ---------------------------------------------------------
    // switch categories
    var $panelPortfolio = $('#page-panel-portfolio');
    if ($panelPortfolio.length) {
        var $portfolio = $('#portfolio');

        // PRETTY-PHOTO
        var options = prettyPhotoOptions;
        options['slideshow'] = 5000;
        options['autoplay_slideshow'] = false;
        options['show_title'] = false;
        $portfolio.find("a[data-gal='prettyPhoto[portfolio]']").prettyPhoto(options);
        $portfolio.find('.img .zoom').click(function(){
            $(this).closest('.item').find("a[data-gal='prettyPhoto[portfolio]']").click();
        });

        var $portfolioData = $portfolio.clone(true, true);
        var $panelPortfolioNav = $panelPortfolio.find('.page-panel-nav');
        $panelPortfolioNav.find('a').click(function(event){
            event.preventDefault();
            $panelPortfolioNav.find('li').removeClass('active');
            $(this).closest('li').addClass('active');

            // filter
            var selector = $(this).attr('data-filter');
            if (selector == 'all') {
              var $filteredData = $portfolioData.find('.item');
            } else {
              var $filteredData = $portfolioData.find('.item[data-type=' + selector + ']');
            }
            $portfolio.find('.wrapper').quicksand(
                $filteredData, {
                    duration: 600,
                    adjustWidth: 'dynamic'
                },
                function () {
                    $portfolio.find("a[data-fal='prettyPhoto[portfolio]']").prettyPhoto(options);
                    $portfolio.find('.img .zoom').click(function(){
                        $(this).closest('.item').find("a[data-fal='prettyPhoto[portfolio]']").click();
                    });
                }
            );
        });
    }


    // PANEL-CLIENTS -----------------------------------------------------------
    // visualize counters
    var $panelClients = $('#page-panel-clients');
    if ($panelClients.length) {
        var $clientsCounters = $panelClients.find('.counters');
        $clientsCounters.appear();
        var clientsCountersReady = false;
        $clientsCounters.on('appear', function(event, $all_appeared_elements) {
            if (clientsCountersReady) return;
            clientsCountersReady = true;
            $clientsCounters.find('.counter-value').each(function(){
                $(this).countTo({
                    from: 0,
                    to: $(this).attr('data-counter'),
                    speed: 2000,
                    refreshInterval: 100
                });
            });
        });
    }

    // FEEDBACK ---------------------------------------------------------------
    // icons in input fields
    $('.feedback .has-icon input').focus(function () {
        $(this).closest('.input-group').find('.input-group-addon').addClass('focused');
    });
    $('.feedback .has-icon input').focusout(function () {
        $(this).closest('.input-group').find('.input-group-addon').removeClass('focused');
    });
	$('.feedback form').validate({
        errorPlacement: function(error, element) {
            $(element).closest('.form-group').append(error);
        },
        errorElement: 'span',
		rules: {
			feedbackName: {
				required: true,
				minlength: 5
			},
			feedbackEmail: {
				required: true,
				email: true
			},
			feedbackMessage: {
				required: true,
				minlength: 15
			}
		},
		messages: {
			feedbackName: {
				required: "This field is required.",
				minlength: "your name must consist of at least 5 characters"
			},
			feedbackEmail: {
				required: "Please enter a valid e-mail address."
			},
			feedbackMessage: {
				required: "This field is required.",
				minlength: "must contain at least 15 characters"
			}
		},
		submitHandler: function(form) {
            $('.feedback .alert-success').addClass('hidden').fadeOut();
            $('.feedback .alert-danger').addClass('hidden').fadeOut();
			$(form).ajaxSubmit({
				type: "POST",
				data: $(form).serialize(),
				url: "mail.php",
				success: function(responseText, statusText, xhr) {
                    console.log(responseText)
                    if (responseText == 'ok'){
                        $('.feedback form').fadeTo("slow", 0.5, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                        });
                        $('.feedback .alert-success').removeClass('hidden').fadeIn();
                    }
                    else {
                        $('.feedback .alert-danger').removeClass('hidden').fadeIn();
                    }
				},
				error: function() {
				    $('.feedback .alert-danger').removeClass('hidden').fadeIn();
				}
			});
		}
	});


    // FIT THUMBNAILS ----------------------------------------------------------
    $(".img-liquid").imgLiquid();


    // PRELOADER ---------------------------------------------------------------
    $('body').queryLoader2({
        backgroundColor: '#fff',
        barColor: '#333',
        barHeight: 3,
        percentage: true
    });

// STELLAR -----------------------------------------------------------------
    $(window).load(function(){
        $(window).stellar();
    });
    $(window).resize(function(){
        $(window).stellar();
    });



    // ON LOAD -----------------------------------------------------------------
    $(window).load(function() {
	    $('#preloader').fadeOut().remove();
    });

});