var isMobile = false;

(function( $ ) {
    "use strict";

    $(function() {

        /* --- MOBILE DETECT --- */
        if (navigator.userAgent.match(/Android/i) ||
            navigator.userAgent.match(/webOS/i) ||
            navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPad/i)||
            navigator.userAgent.match(/iPod/i) ||
            navigator.userAgent.match(/BlackBerry/i)) {
            isMobile = true;
        }

        /* --- ADD NECESSARY CLASS --- */
        if (isMobile == true) {
            $('.animated').removeClass('animated');
            $('.op0').removeClass('op0');
        }

        /* --- ANIMATE CONTENT --- */
        if (isMobile == false) {
            $('*[data-animated]').addClass('animated');
        }

        function animated_contents() {
            $(".animated:appeared").each(function (i) {
                var $this    = $(this),
                    animated = $(this).data('animated');

                setTimeout(function () {
                    $this.addClass(animated);
                }, 100 * i);

            });
        }

        if (isMobile == false) {

            animated_contents();
            $(window).scroll(function () {
                animated_contents();
            });
        }
    });

}(jQuery));