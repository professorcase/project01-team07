using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;
using System.Security.Claims;
using System;

namespace Project01_Team07.Controllers
{
    public class EnrollmentsController : Controller
    {
        private ApplicationDbContext _context;

        public EnrollmentsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Enrollments
        public IActionResult Index()
        {
            return View(_context.Enrollments.ToList());
        }

        // GET: Enrollments/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Enrollment enrollment = _context.Enrollments.Single(m => m.EnrollmentID == id);
            if (enrollment == null)
            {
                return HttpNotFound();
            }

            return View(enrollment);
        }

        // GET: Enrollments/Create
        public IActionResult Create()
        {
            var sections = _context.Section.Where(m => m.isAssigned == true).ToList();
            var s = _context.Students.Single(m => m.email == User.GetUserName());
            ViewBag.Sections = new MultiSelectList(sections, "sectionCRN", "sectionName");
            ViewData["StudentID"] = s.StudentID;
            return View();
        }

        // POST: Enrollments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Enrollment enrollment)
        {
            var c = HttpContext.Request;
            if (ModelState.IsValid)
            {
                int count = 0;
                var crns = c.Form["sectionCRN"];
                foreach (string crn in crns)
                {
                    count++;
                    if (count == 1)
                    {
                        enrollment.crn1 = Int32.Parse(crn);
                    }
                    else if (count == 2)
                    {
                        enrollment.crn2 = Int32.Parse(crn);
                    }
                    else
                    {
                        enrollment.crn3 = Int32.Parse(crn);
                    }
                }
                _context.Enrollments.Add(enrollment);
                _context.SaveChanges();
                var s = _context.Students.Single(m => m.email == User.GetUserName());
                s.isEnrolled = true;
                _context.Update(s);
                _context.SaveChanges();
                return RedirectToAction("Index","Students");
            }
            return View(enrollment);
        }

        // GET: Enrollments/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Enrollment enrollment = _context.Enrollments.Single(m => m.EnrollmentID == id);
            if (enrollment == null)
            {
                return HttpNotFound();
            }
            return View(enrollment);
        }

        // POST: Enrollments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Enrollment enrollment)
        {
            if (ModelState.IsValid)
            {
                _context.Update(enrollment);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(enrollment);
        }

        // GET: Enrollments/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Enrollment enrollment = _context.Enrollments.Single(m => m.EnrollmentID == id);
            if (enrollment == null)
            {
                return HttpNotFound();
            }

            return View(enrollment);
        }

        // POST: Enrollments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Enrollment enrollment = _context.Enrollments.Single(m => m.EnrollmentID == id);
            _context.Enrollments.Remove(enrollment);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
