﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Project01_Team07.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Project01_Team07.Controllers
{
    public class LandingController : Controller
    {
        private ApplicationDbContext dbContext;

        public LandingController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        internal object Delete(object p)
        {
            throw new NotImplementedException();
        }

        internal object Details(object p)
        {
            throw new NotImplementedException();
        }

        internal object Create()
        {
            throw new NotImplementedException();
        }
    }
}
