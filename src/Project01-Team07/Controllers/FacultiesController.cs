﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;
using System.Security.Claims;

namespace Project01_Team07.Controllers
{
    public class FacultiesController : Controller
    {
        private ApplicationDbContext _context;

        public FacultiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Faculties
        public IActionResult Index()
        {

            var faculty = _context.Faculties.Single(m => m.email == User.GetUserName());
            if (faculty.isFacultyEnrolled)
            {

                var enrollment = _context.Facultyenrollment.Single(m => m.FacultyID == faculty.FacultyID);
                ViewData["crn1"] = enrollment.crn1;
                ViewData["crn2"] = enrollment.crn2;
                ViewData["crn3"] = enrollment.crn3;
            }
            return View(faculty);
        }

        //// POST: Faculties
        //public IActionResult Index()
        //{
        //    return View("Feedback");
        //}

        public IActionResult Feedback()
        {
            return View();
        }

        // GET: Faculties/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculties.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }

            return View(faculty);
        }

        // GET: Faculties/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Faculties/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Faculty faculty)
        {
            if (ModelState.IsValid)
            {
                _context.Faculties.Add(faculty);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        // GET: Faculties/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculties.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            return View(faculty);
        }

        // POST: Faculties/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Faculty faculty)
        {
            if (ModelState.IsValid)
            {
                _context.Update(faculty);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        // GET: Faculties/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculties.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }

            return View(faculty);
        }

        // POST: Faculties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Faculty faculty = _context.Faculties.Single(m => m.FacultyID == id);
            _context.Faculties.Remove(faculty);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}