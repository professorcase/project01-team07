﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Project01_Team07.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace Project01_Team07.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public HomeController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            if (User.IsSignedIn())
            {
                var user = await _userManager.FindByNameAsync(User.GetUserName());
                if (user != null)
                {
                    var cuser = await _userManager.FindByIdAsync(user.Id);

                    if (await _userManager.IsInRoleAsync(cuser, "student"))
                    {
                        var s = _context.Students.Single(m => m.email == user.Email);
                        return RedirectToAction("Index", "Students");
                    }
                    else if (await _userManager.IsInRoleAsync(cuser, "faculty"))
                    {
                        var f = _context.Faculties.Single(m => m.email == user.Email);
                        return RedirectToAction("Index", "Faculties");
                    }
                }
            }
            return View();
        }


        public IActionResult FacultyHome()
        {

            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Milestone()
        {

            return View();
        }

        public IActionResult FutureWork()
        {

            return View();
        }

        public IActionResult Preethi()
        {

            return View();
        }

        public IActionResult Srikanth()
        {

            return View();
        }

        public IActionResult Pradeep()
        {

            return View();
        }
        public IActionResult Priyanka()
        {

            return View();
        }
        public IActionResult Manoj()
        {

            return View();
        }

        public IActionResult Rahul()
        {

            return View();
        }
    }
}
