using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;

namespace Project01_Team07.Controllers
{
    public class FeedbackHistoriesController : Controller
    {
        private ApplicationDbContext _context;

        public FeedbackHistoriesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: FeedbackHistories
        public IActionResult Index()
        {
            return View(_context.FeedbackHistory.ToList());
        }

        // GET: FeedbackHistories/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FeedbackHistory feedbackHistory = _context.FeedbackHistory.Single(m => m.FeedbackHistoryID == id);
            if (feedbackHistory == null)
            {
                return HttpNotFound();
            }

            return View(feedbackHistory);
        }

        // GET: FeedbackHistories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FeedbackHistories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FeedbackHistory feedbackHistory)
        {
            if (ModelState.IsValid)
            {
                _context.FeedbackHistory.Add(feedbackHistory);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedbackHistory);
        }

        // GET: FeedbackHistories/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FeedbackHistory feedbackHistory = _context.FeedbackHistory.Single(m => m.FeedbackHistoryID == id);
            if (feedbackHistory == null)
            {
                return HttpNotFound();
            }
            return View(feedbackHistory);
        }

        // POST: FeedbackHistories/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FeedbackHistory feedbackHistory)
        {
            if (ModelState.IsValid)
            {
                _context.Update(feedbackHistory);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedbackHistory);
        }

        // GET: FeedbackHistories/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FeedbackHistory feedbackHistory = _context.FeedbackHistory.Single(m => m.FeedbackHistoryID == id);
            if (feedbackHistory == null)
            {
                return HttpNotFound();
            }

            return View(feedbackHistory);
        }

        // POST: FeedbackHistories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            FeedbackHistory feedbackHistory = _context.FeedbackHistory.Single(m => m.FeedbackHistoryID == id);
            _context.FeedbackHistory.Remove(feedbackHistory);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
