using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;
using System.Security.Claims;
using System;

namespace Project01_Team07.Controllers
{
    public class FeedbacksController : Controller
    {
        private ApplicationDbContext _context;

        public FeedbacksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Feedbacks
        public IActionResult Index()
        {
            return View(_context.Feedback.ToList());
        }

        // GET: Daily
        public IActionResult Daily(string crn)
        {
            var student = _context.Students.Single(m => m.email == User.GetUserName());
            var fbHistory = _context.FeedbackHistory.SingleOrDefault(m => m.StudentID == student.StudentID.ToString() && m.type == "Daily" && m.CreatedUtc == DateTime.Today && m.crn == crn);
            if (fbHistory == null)
            {
                ViewData["section"] = crn;
                return View();
            }
            else {
                return RedirectToAction("Already");
            }
        }
        // POST: Students/Feedback
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DailySave()
        {
            try
            {

                var student = _context.Students.Single(m => m.email == User.GetUserName());
                var c = HttpContext.Request;
                System.Diagnostics.Debug.WriteLine("==========================");
                System.Diagnostics.Debug.WriteLine(c.Form["q1"].ToString());
                System.Diagnostics.Debug.WriteLine(c.Form["comments"].ToString());
                Feedback feedback = new Feedback();
                feedback.FeedbackResponse = c.Form["q1"].ToString() + "," + c.Form["q2"].ToString() + "," + c.Form["q3"].ToString();
                feedback.crn = c.Form["crn"].ToString();
                feedback.FeedbackComment = c.Form["comments"].ToString();
                feedback.CreatedUtc = DateTime.Today;
                feedback.type = "Daily";
                _context.Feedback.Add(feedback);
                _context.SaveChanges();
                FeedbackHistory feedbackHistory = new FeedbackHistory();
                feedbackHistory.StudentID = student.StudentID + "";
                feedbackHistory.type = "Daily";
                feedbackHistory.crn = c.Form["crn"].ToString();
                feedbackHistory.CreatedUtc = DateTime.Today;
                _context.FeedbackHistory.Add(feedbackHistory);
                _context.SaveChanges();
                return RedirectToAction("Success");
            }
            catch (System.Exception e)
            {
                return RedirectToAction("Failure");
            }

        }



        // GET: Month
        public IActionResult Month(string crn)
        {
            var student = _context.Students.Single(m => m.email == User.GetUserName());
            var fbHistory = _context.FeedbackHistory.SingleOrDefault(m => m.StudentID == student.StudentID.ToString() && m.type == "Month" && m.CreatedUtc == DateTime.Today && m.crn == crn);
            if (fbHistory == null)
            {
                ViewData["section"] = crn;
                return View();
            }
            else {
                return RedirectToAction("Already");
            }
        }
        // POST: Students/Feedback
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult MonthSave()
        {
            try
            {
                var student = _context.Students.Single(m => m.email == User.GetUserName());
                var c = HttpContext.Request;
                System.Diagnostics.Debug.WriteLine("==========================");
                System.Diagnostics.Debug.WriteLine(c.Form["q1"].ToString());
                System.Diagnostics.Debug.WriteLine(c.Form["comments"].ToString());
                Feedback feedback = new Feedback();
                feedback.FeedbackResponse = c.Form["q1"].ToString() + "," + c.Form["q2"].ToString() + "," + c.Form["q3"].ToString();
                feedback.crn = c.Form["crn"].ToString();
                feedback.FeedbackComment = c.Form["comments"].ToString();
                feedback.CreatedUtc = DateTime.Today;
                feedback.type = "Month";
                _context.Feedback.Add(feedback);
                _context.SaveChanges();


                FeedbackHistory feedbackHistory = new FeedbackHistory();
                feedbackHistory.StudentID = student.StudentID + "";
                feedbackHistory.type = "Month";
                feedbackHistory.crn = c.Form["crn"].ToString();
                feedbackHistory.CreatedUtc = DateTime.Today;
                _context.FeedbackHistory.Add(feedbackHistory);
                _context.SaveChanges();

                return RedirectToAction("Success");
            }
            catch (System.Exception e)
            {
                return RedirectToAction("Failure");
            }
        }

        // GET: Semister
        public IActionResult Semister(string crn)
        {
            var student = _context.Students.Single(m => m.email == User.GetUserName());
            var fbHistory = _context.FeedbackHistory.SingleOrDefault(m => m.StudentID == student.StudentID.ToString() && m.type == "Semister" && m.CreatedUtc == DateTime.Today && m.crn == crn);
            if (fbHistory == null)
            {
                ViewData["section"] = crn;
                return View();
            }
            else {
                return RedirectToAction("Already");
            }
        }
        // POST: Students/Feedback
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SemisterSave()
        {
            try
            {
                var c = HttpContext.Request;
                System.Diagnostics.Debug.WriteLine("==========================");
                System.Diagnostics.Debug.WriteLine(c.Form["q1"].ToString());
                System.Diagnostics.Debug.WriteLine(c.Form["comments"].ToString());
                Feedback feedback = new Feedback();
                feedback.FeedbackResponse = c.Form["q1"].ToString() + "," + c.Form["q2"].ToString() + "," + c.Form["q3"].ToString();
                feedback.crn = c.Form["crn"].ToString();
                feedback.FeedbackComment = c.Form["comments"].ToString();
                feedback.CreatedUtc = DateTime.Today;
                feedback.type = "Semister";
                _context.Feedback.Add(feedback);
                _context.SaveChanges();

                var student = _context.Students.Single(m => m.email == User.GetUserName());
                FeedbackHistory feedbackHistory = new FeedbackHistory();
                feedbackHistory.StudentID = student.StudentID + "";
                feedbackHistory.type = "Semister";
                feedbackHistory.crn = c.Form["crn"].ToString();
                feedbackHistory.CreatedUtc = DateTime.Today;
                _context.FeedbackHistory.Add(feedbackHistory);
                _context.SaveChanges();
                return RedirectToAction("Success");
            }
            catch (System.Exception e)
            {
                return RedirectToAction("Failure");
            }
        }

        // GET: Success
        public IActionResult Success()
        {
            return View();
        }

        // GET: Success
        public IActionResult Already()
        {
            return View();
        }

        // GET: Failure
        public IActionResult Failure()
        {
            return View();
        }

        // GET: Feedbacks/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedback.Single(m => m.FeedbackID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }

            return View(feedback);
        }

        // GET: Feedbacks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Feedbacks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                _context.Feedback.Add(feedback);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedback);
        }

        // GET: Feedbacks/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedback.Single(m => m.FeedbackID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // POST: Feedbacks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                _context.Update(feedback);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feedback);
        }

        // GET: Feedbacks/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Feedback feedback = _context.Feedback.Single(m => m.FeedbackID == id);
            if (feedback == null)
            {
                return HttpNotFound();
            }

            return View(feedback);
        }

        // POST: Feedbacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Feedback feedback = _context.Feedback.Single(m => m.FeedbackID == id);
            _context.Feedback.Remove(feedback);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
