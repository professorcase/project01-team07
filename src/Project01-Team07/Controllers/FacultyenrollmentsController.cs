using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;
using System.Security.Claims;
using System;

namespace Project01_Team07.Controllers
{
    public class FacultyenrollmentsController : Controller
    {
        private ApplicationDbContext _context;

        public FacultyenrollmentsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Facultyenrollments
        public IActionResult Index()
        {
            return View(_context.Facultyenrollment.ToList());
        }

        // GET: Facultyenrollments/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Facultyenrollment facultyenrollment = _context.Facultyenrollment.Single(m => m.facEnrollmentID == id);
            if (facultyenrollment == null)
            {
                return HttpNotFound();
            }

            return View(facultyenrollment);
        }

        // GET: Facultyenrollments/Create
        public IActionResult Create()
        {
        var f = _context.Faculties.Single(m => m.email == User.GetUserName());
        var sections = _context.Section.Where(m=>m.isAssigned==false).ToList();
        ViewBag.Sections = new MultiSelectList(sections, "sectionCRN", "sectionName");
        ViewData["FacultyID"] = f.FacultyID;
        return View();
        }

        // POST: Facultyenrollments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Facultyenrollment facultyenrollment)
        {
            var c = HttpContext.Request;
            if (ModelState.IsValid)
            {
                int count = 0;
                var crns = c.Form["sectionCRN"];
                foreach (string crn in crns)
                {
                    count++;
                    if(count == 1)
                    {
                        facultyenrollment.crn1 = Int32.Parse(crn);
                    }else if (count ==2)
                    {
                        facultyenrollment.crn2 = Int32.Parse(crn);
                    }else
                    {
                        facultyenrollment.crn3 = Int32.Parse(crn);
                    }
                }
                

                _context.Facultyenrollment.Add(facultyenrollment);
                _context.SaveChanges();
                var crn1 = _context.Section.Single(m => m.sectionCRN == facultyenrollment.crn1.ToString());
                crn1.isAssigned = true;
                _context.Update(crn1);
                _context.SaveChanges();
                var crn2 = _context.Section.Single(m => m.sectionCRN == facultyenrollment.crn2.ToString());
                crn2.isAssigned = true;
                _context.Update(crn2);
                _context.SaveChanges();
                var crn3 = _context.Section.Single(m => m.sectionCRN == facultyenrollment.crn3.ToString());
                crn3.isAssigned = true;
                _context.Update(crn3);
                _context.SaveChanges();

                var s = _context.Faculties.Single(m => m.email == User.GetUserName());
                s.isFacultyEnrolled = true;
                _context.Update(s);
                _context.SaveChanges();

            return RedirectToAction("Index","Faculties");
            }
            return View(facultyenrollment);
        }

        // GET: Facultyenrollments/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Facultyenrollment facultyenrollment = _context.Facultyenrollment.Single(m => m.facEnrollmentID == id);
            if (facultyenrollment == null)
            {
                return HttpNotFound();
            }
            return View(facultyenrollment);
        }

        // POST: Facultyenrollments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Facultyenrollment facultyenrollment)
        {
            if (ModelState.IsValid)
            {
                _context.Update(facultyenrollment);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(facultyenrollment);
        }

        // GET: Facultyenrollments/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Facultyenrollment facultyenrollment = _context.Facultyenrollment.Single(m => m.facEnrollmentID == id);
            if (facultyenrollment == null)
            {
                return HttpNotFound();
            }

            return View(facultyenrollment);
        }

        // POST: Facultyenrollments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Facultyenrollment facultyenrollment = _context.Facultyenrollment.Single(m => m.facEnrollmentID == id);
            _context.Facultyenrollment.Remove(facultyenrollment);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
