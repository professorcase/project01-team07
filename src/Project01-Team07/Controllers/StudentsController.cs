using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Project01_Team07.Models;
using System.Security.Claims;
using System;

namespace Project01_Team07.Controllers
{
    public class StudentsController : Controller
    {
        private ApplicationDbContext _context;

        public StudentsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Students
        public IActionResult Index()
        {
            var student = _context.Students.Single(m => m.email == User.GetUserName());
            if (student.isEnrolled)
            {
                var crnss = _context.Section.Where(m => m.isAssigned == true).ToList();
                var enrollment = _context.Enrollments.Single(m => m.StudentID == student.StudentID);
                ViewData["crn1"] = enrollment.crn1;
                ViewData["crn2"] = enrollment.crn2;
                ViewData["crn3"] = enrollment.crn3;
            }
            return View(student);
        }

        // POST: Students/Feedback
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Feedback()
        {
            var c = HttpContext.Request;
            System.Diagnostics.Debug.WriteLine("==========================");
            System.Diagnostics.Debug.WriteLine(c.Form["crn"].ToString());
            System.Diagnostics.Debug.WriteLine(c.Form["feedbackType"].ToString());
            string crn = c.Form["crn"].ToString();
            ViewData["section"]= c.Form["crn"].ToString();
            if (c.Form["feedbackType"].ToString().Equals("daily"))
            {
                return RedirectToAction("Daily", "Feedbacks",new { crn = ViewData["section"] });
            }else if (c.Form["feedbackType"].ToString().Equals("monthly"))
            {
                return RedirectToAction("Month", "Feedbacks", new { crn = ViewData["section"] });
            }
            else if (c.Form["feedbackType"].ToString().Equals("semister"))
            {
                return RedirectToAction("Semister", "Feedbacks", new { crn = ViewData["section"] });
            }
            else
            {
                return View();
            }
            
        }

        // GET: Students/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Student student = _context.Students.Single(m => m.StudentID == id);
            if (student == null)
            {
                return HttpNotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Students.Add(student);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Student student = _context.Students.Single(m => m.StudentID == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Update(student);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Students/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Student student = _context.Students.Single(m => m.StudentID == id);
            if (student == null)
            {
                return HttpNotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Student student = _context.Students.Single(m => m.StudentID == id);
            _context.Students.Remove(student);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
