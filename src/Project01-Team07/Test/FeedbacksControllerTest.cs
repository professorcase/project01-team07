﻿using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using Project01_Team07.Controllers;
using Project01_Team07.Models;
using Microsoft.Data.Entity;
using Xunit;

namespace Project01_Team07.Test
{
    public class FeedbacksControllerTest
    {

        private readonly IServiceProvider _serviceProvider;
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

     

        public FeedbacksControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework().AddInMemoryDatabase().AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());
            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void CourseFeedback_CreatesView()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new FeedbacksController(dbContext);
            var result = controller.Create();
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        [Fact]
        public void CourseFeedback_DetailsViewReturn()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new FeedbacksController(dbContext);
            var result = controller.Details(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
        }

        [Fact]
        public void CourseFeedback_Delete()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new FeedbacksController(dbContext);
            var result = controller.Delete(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
        }

        private IActionResult View()
        {
            throw new NotImplementedException();
        }
    }
}
