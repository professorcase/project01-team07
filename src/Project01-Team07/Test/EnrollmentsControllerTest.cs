﻿using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using Project01_Team07.Controllers;
using Project01_Team07.Models;
using Microsoft.Data.Entity;
using Xunit;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Project01_Team07.Test
{
    public class EnrollmentsControllerTest : Controller
    {

        private readonly IServiceProvider _serviceProvider;
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public EnrollmentsControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework().AddInMemoryDatabase().AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());
            _serviceProvider = services.BuildServiceProvider();
        }

       
        [Fact]
        public void CourseFeedback_DetailsViewReturn()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new EnrollmentsController(dbContext);
            var result = controller.Details(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
        }

        [Fact]
        public void CourseFeedback_Delete()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new EnrollmentsController(dbContext);
            var result = controller.Delete(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
        }
    }
}
